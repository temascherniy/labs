﻿// Laba1 Fixed.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>

using namespace std;
int geron(int sideA, int sideB, int sideC) {
	int result;
	int P;

	cout << "Input your sides a,b,c" << endl;
	cin >> sideA;
	cin >> sideB;
	cin >> sideC;
	P = (sideA + sideB + sideC) / 2;
	result = sqrt(P * (P - sideA) * (P - sideB) * (P - sideC));
	cout << "Your S:" << result << endl;

	return 0;
}

void inPow() {
	int num;
	int inPow;
	cout << " Input your num for pow 8  " << endl;
	cin >> num;
	inPow = pow(num, 8);
	cout << "Result:" << inPow << endl;
}

void bites() {
	int a = sizeof(short);
	int b = sizeof(int);
	int c = sizeof(long);
	int d = sizeof(char);
	int e = sizeof(float);
	int f = sizeof(double);
	cout << "Your bites:" << endl;
	std::cout << "Size of short = " << a << endl
		<< "Size of int = " << b << endl
		<< "Size of long = " << c << endl
		<< "Size of char = " << d << endl
		<< "Size of flaot = " << e << endl
		<< "Size of double = " << f << endl;
}

void swap() {
	cout << "Swap with help of additional arg" << endl;
	int a, b;

	cout << "Enter two values: ";
	cin >> a >> b;
	cout << "Value  before: a = " << a << ", b = " << b << std::endl;
	int tmp = a;
	a = b;
	b = tmp;
	cout << "Value now: a = " << a << ", b = " << b << std::endl;
}

void swapAd() {
	cout << "Swap without additional arg" << endl;
	int a, b;
	cout << "Enter two values: ";
	cin >> a >> b;
	cout << "Value  before: a = " << a << ", b = " << b << std::endl;
	swap(a, b);
	cout << "Value now: a = " << a << ", b = " << b << std::endl;
}

void progDeceiver() {
	int yourNum;
	int botNum;
	cout << " Input your num:  ";
	cin >> yourNum;
	botNum = yourNum + 2;
	cout << "Bot num is  " << botNum << endl;
	if (botNum > yourNum) {
		cout << "Bot win" << endl;
	}
	else {
		cout << "You win";
	}

}

int main()
{
	geron(3, 4, 5);
	inPow();
	bites();
	swap();
	swapAd();
	progDeceiver();
	return 0;
}
