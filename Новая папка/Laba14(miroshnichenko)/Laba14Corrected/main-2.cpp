#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "main-2.h"

int get_number() {
	int b;
	printf("������� �����>");
	scanf("%d", &b);
	return b;
}


int ent_data(MON* m) {
	float sqx;

	printf("������� ������������, ���, �������� ������� (��) , ����������� (�/��)>");
	scanf("%s %s %d %f", m->name, &m->sc, &m->cnt, &sqx);
	m->sq = sqx;
	if (!strcmp(m->name, "***")) return -1;

	
	return 0;

}

void show_1(MON* m) {
	printf("\n����������         : %s\n", m->name);
	printf("���              : ", m->sc);
	
	printf("\n�����.����.  : %d\n", m->cnt);
	printf("����������� : %6.2f\n", m->sq);
	print_line();
}

void show_row(MON* m) {
	printf("|%5s      |%5c  | %5d     | %-5.1f       |\n",
		m->name, m->sc, m->cnt, m->sq);
}

void print_line() {
	printf("---------------------------------------------\n");
}

void print_head() {
	print_line();
	printf("|	   	�������������������� ��������         |\n");
	printf("|---------------------------------------------|\n");
	printf("|����������.|  ���  |�����.����.|�����������  |\n");
	printf("|-----------|-------|-----------|-------------|\n");
}