#include <iostream>
using namespace std;
int main()
{
    const int max = 50;
    int arr[max], temp[max];
 
    for (int i=0; i<max; i++)
    {
        arr[i] = rand() % 101;
        temp[i] = arr[i];
    }
 
    for (int i=0; i< max; i++)
    {
        for (int j=0; j<max; j++ )
        {
            int buf;
            if (temp[j] < temp[j+1])
            {
                buf = temp[j];
                temp[j] = temp[j+1];
                temp[j+1]=buf;
            }
        }
 
    }
 
    cout << endl;
    for (int i=0; i<max; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
 
    int count = 0;
    int tret = 0;
    for (int i=max; i>0; i--)
    {
        if (temp[i-1] != temp[i])
        {
            count++;
            if (count == 3)
                tret = temp[i-1];
        }
    }
 
        cout << "Третье по величине число: " << tret << endl;
        cout << endl << "Преобразованный массив: "<< endl;
        for (int i=0; i<max; i++)
        {
            if (arr[i] > tret)
                arr[i] = tret;
            cout << arr[i] << " ";
        }
        cout << endl;
    
    return 0;
} 
