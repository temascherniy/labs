#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::srand;
using std::rand;

#include <iomanip>
using std::setw;

#include <ctime>


int main() {

    int const a = 9;
    int temp;
    int matrix[a][a];
		
    srand(time(NULL));
    

    for(int i = 0; i<a; ++i){
        for(int j = 0; j<a; ++j){
            matrix[i][j] = rand() % 100;
            cout << setw(3)<< matrix[i][j] << " ";
        }
        cout << endl;
    }
    for(int i = 0; i<a; ++i){
        int temp = matrix[i][i];
        matrix[i][i] = matrix[i][(a-1)-i];
        matrix[i][(a-1)-i] = temp;
    }

    cout << endl;
    for(int i = 0; i<a; ++i){
        for(int j = 0; j<a; ++j){
            cout << setw(3) << matrix[i][j] << " ";
        }
        cout << endl;
    }

    cin.get();

}
