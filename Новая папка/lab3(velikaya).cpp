#include <math.h>
#include <stdio.h>


int main(void) {
  double x, y;
  double a = 7.5;
  double b = 5.2;
  double c = 3.8;
  double t1, t2;
  double ax, co, tg;

  printf("Введите x, y >");
  scanf("%lf %lf", &x, &y);

  ax = a * y;
  co = cos(x);
  tg = tan(ax / 2);

  t1 = ((1 / ax + b) + (y / c) * log((y * x + a) / (ax + b))) / c;
  t2 = sin(ax) / 2 * a * pow(co, 2) + log(tg) / 2 *a;

printf("t1 = %lg\n", t1);
printf("t2 = %lg\n", t2);

  return 0;
}
