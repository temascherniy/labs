 
#include <math.h>
#include <stdio.h>


int main(void) {
  double x, y;
  double a = 3.2;
  double b = 6.4;
  double t1, t2;
  double ax, tg, l;

  printf("Введите x, y >");
  scanf("%lf %lf", &x, &y);

  ax = a * x;
  tg = tan(ax / 2);
  l = log(y/x)-pow(ax, 2)/2*pow(y, 2);

  t1 = 1/pow(b, 3)*l;
  t2 = 1/a*tg+1/a*log(tg);

printf("t1 = %lg\n", t1);
printf("t2 = %lg\n", t2);

  return 0;
}
