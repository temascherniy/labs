#include <iostream>
#include <math.h>

using namespace std;

int main()
{
 long n;
 double dbln;
 double sum=0;
 double term;
 const double eps=0.000001;
 short k1=1;
 for (n=0; ; n++, k1=-k1){
  dbln=n;
  term=k1*((dbln+1)/(dbln*dbln*dbln-dbln*dbln+1);
  if (fabs(term)>=eps) 
   sum+=term;
   else break;
   if (n==9)       
        cout<<"Сумма 10 членов ряда = "<< sum;
 }
  cout<<"Полная сумма ряда = "<< sum;    
  return 0;
} 
